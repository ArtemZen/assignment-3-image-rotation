#ifndef ASSIG
#define ASSIG
#include "image.h"

enum read_status
{
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum read_status from_bmp (FILE *in, struct image **image);

enum write_status
{
    WRITE_OK = READ_INVALID_HEADER,
    WRITE_HEADER_ERROR,
    WRITE_ERROR
};

enum write_status to_bmp (FILE *out, struct image const *image);
struct bmp_header build_bmp_header (size_t bmp_file_size, size_t width,
                                    size_t height, size_t image_size);
static char *Message_read_error[]
    = { "No errors of reading bmp file!", "Invalid reading signature!",
        "Invalid reading bits of bmp image!",
        "Invalid reading header of bmp image!" };
static char *Message_write_error[]
    = { "No errors of writing bmp file!",
        "Error while writting header of out bmp file!",
        "Error while writting of out bmp file!" };
#endif // ASSIG

