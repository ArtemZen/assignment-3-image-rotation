#ifndef ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


#pragma pack(push, 1)
struct __attribute__((__packed__)) bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

struct __attribute__((__packed__)) pixel { uint8_t b, g, r; };

struct __attribute__((__packed__)) image {
    uint64_t width, height;
    struct pixel* data;
};

int64_t pad_img(size_t width);

struct image* cr_img(uint64_t width, uint64_t height);

void des_img(struct image* image);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H

