#ifndef ENT_ERR
#define ENT_ERR
enum file_finding_status {
    file_exists = 0,
    no_readfile,
    no_writefile,
    no_enough_files

};
static char* Entry_errors[] = {
    "Entry is ok!",
    "Invalid path to the readfile entry!",
    "Invalid path to the writefile entry!",
    "Invalid enough files!"
};
#endif //ENT_ERR
