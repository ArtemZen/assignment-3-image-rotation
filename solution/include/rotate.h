#ifndef IMG_TRANS_ROT
#define IMG_TRANS_ROT

#include "image.h"

struct image* rotate(struct image* img);

#endif //IMG_TRANS_ROT
