#include "entry.h"
#include "bmp.h"
#include "image.h"
#include "rotate.h"
#include <stdio.h>
#define check_not_equal(a, b, c) if((a)!=(b)){printf("%s", c);\
        return 1;}
#define check_equal(a, b, c) if((a)==(b)){printf("%s", c);\
        return 1;}
int main(int argc, char** argv) {
    check_not_equal(argc, 3, Entry_errors[no_enough_files]);
    FILE* f_r = fopen(argv[1], "rb");
    FILE* f_w = fopen(argv[2], "wb");
    check_equal(f_r, NULL, Entry_errors[no_readfile]);
    check_equal(f_w, NULL, Entry_errors[no_writefile]);
    struct image* m_image;
    enum read_status r_s = from_bmp(f_r, &m_image);
    check_not_equal(r_s, 0, Message_read_error[r_s]);
    struct image* r = rotate(m_image);
    des_img(m_image);
    enum write_status w_stat = to_bmp(f_w, r);
    check_not_equal(w_stat, WRITE_OK, Message_write_error[w_stat]);
    des_img(r);
    check_not_equal(fclose(f_r), 0, "Can't close read file!");
    check_not_equal(fclose(f_w), 0, "Can't close write file!");
    return 0;
}

