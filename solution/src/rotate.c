#include "image.h"
#include "rotate.h"

struct image *
rotate (struct image *img)
{ //Функция создающая перевернутую картинку из данной
    struct image *r = cr_img (img->height, img->width);
    for (uint64_t i = 0; i < r->height; i++)
        {
            for (uint64_t l = 0; l < r->width; l++)
                {
                    r->data[i * r->width + l]
                        = img->data[(img->height - 1 - l) * img->width
                                    + i]; //Главный адгоритм построения
                                          //перевернутого изображения
                }
        }
    return r;
}
