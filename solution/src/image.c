#include "image.h"

struct image * cr_img (uint64_t w, uint64_t h)
{ 
    struct image *img = malloc (sizeof (struct image));
    img->width = w;
    img->height = h;
    img->data = malloc (sizeof (struct pixel) * (w * h));
    return img;
}

void des_img (struct image *image)
{
    free (image->data);
    free (image);
}

int64_t pad_img (size_t w)
{
    if (w % 4)
        {
            return(4 - (int64_t)(w * sizeof (struct pixel) % 4));
        }
    return 0;
}
