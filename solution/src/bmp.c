#include "bmp.h"

#define BFTYOPE 0x4d42
#define BOFFBITS 54
#define BISIZE 40
#define BIBITCOUNT 24

enum read_status
from_bmp (FILE *in_bmp_file, struct image **image_main)
{
    struct bmp_header main_header = { 0 };
    uint8_t status
        = fread (&main_header, sizeof (struct bmp_header), 1,
                 in_bmp_file); //Внутри main_header записываем header bmp файла
    if (!status)
        { //Проверяем файл на ошибку
            return READ_INVALID_HEADER;
        }
    *image_main = cr_img (main_header.biWidth,
                          main_header.biHeight); //Создаем объект картинки
    const int64_t img_main_padding
        = pad_img (main_header.biWidth); //Получаем количество дополнительных
                                         //пикселей для будущего изображения
    uint64_t size = 0;
    for (int64_t i = 0; i < main_header.biHeight; i++)
        {
            for (int64_t l = 0; l < main_header.biWidth; l++)
                {
                    status = fread (
                        ((*image_main)->data + size), sizeof (struct pixel), 1,
                        in_bmp_file); //Проверяем каждый писель на его наличие
                    if (!status)
                        {
                            des_img (*image_main);
                            return READ_INVALID_BITS;
                        }
                    size++;
                }
            status = fseek (
                in_bmp_file, img_main_padding,
                SEEK_CUR); //Проверяем на наличие дополнительных сиволов
            if (status)
                {
                    des_img (*image_main);
                    return READ_INVALID_SIGNATURE;
                }
        }
    return READ_OK;
}

enum write_status
to_bmp (FILE *out_bmp_file, struct image const *main_image)
{
    const int64_t main_img_padding = pad_img (
        main_image->width); //Получаем количество доаполнительных символов
    const size_t bmp_file_size = main_image->width * sizeof (struct pixel)
                                 + main_img_padding * main_image->height
                                 + sizeof (struct bmp_header);
    size_t image_size = (main_image->width + main_img_padding)
                        * main_image->height; //Вычисляем размер файла
    struct bmp_header main_header = build_bmp_header (
        bmp_file_size, main_image->width, main_image->height,
        image_size); //Формируем згаловок
    uint8_t status = fwrite (
        &main_header, sizeof (struct bmp_header), 1,
        out_bmp_file); //Записываем в файл заголовок и проверяем на ошибки
    if (!status)
        {
            return WRITE_HEADER_ERROR;
        }
    status = fseek (out_bmp_file, main_header.bOffBits,
                    SEEK_SET); // Проверяем на наличие дополнительных пикслей
    if (status)
        {
            return WRITE_ERROR;
        }

    struct pixel *pixels_of_image = main_image->data;
    for (int64_t i = 0; i < main_image->height; i++)
        { //Записываем пиксели поочередно по острочке
            uint64_t size = sizeof (struct pixel) * main_image->width;
            struct pixel *current_str
                = main_image->data + i * main_image->width;
            status = fwrite (current_str, size, 1, out_bmp_file);
            if (!status)
                {
                    return WRITE_ERROR;
                }
            status
                = fwrite (pixels_of_image, main_img_padding, 1, out_bmp_file);
            if (main_img_padding != 0 && !status)
                {
                    return WRITE_ERROR;
                }
        }

    return WRITE_OK;
}
struct bmp_header
build_bmp_header (const size_t bmp_file_size, const size_t width,
                  const size_t height, const size_t image_size)
{
    //Функция построение заголовка
    struct bmp_header header = (struct bmp_header){
        .bfType = BFTYOPE,
        .bfileSize = bmp_file_size,
        .bfReserved = 0,
        .bOffBits = BOFFBITS,
        .biSize = BISIZE,
        .biWidth = width,
        .biHeight = height,
        .biPlanes = 1,
        .biBitCount = BIBITCOUNT,
        .biCompression = 0,
        .biSizeImage = image_size,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0,
    };
    return header;
}

